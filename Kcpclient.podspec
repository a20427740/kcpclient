Pod::Spec.new do |s|
    s.name             = "Kcpclient"
    s.version          = "1.2.2"
    s.summary          = "Kcpclient"
    s.license          = 'LGPLv2.1'
    s.author           = { "littleplayer" => "20727740@qq.com" }
    s.homepage         = "https://bitbucket.org/a20427740/talkfunkcpclient/src/master/"
    s.source           = { :git => "https://a20427740@bitbucket.org/a20427740/talkfunkcpclient.git", :tag => s.version.to_s }

    s.platform     = :ios, '9.0'
    s.requires_arc = true
    s.vendored_frameworks = 'SDK/Kcpclient.framework'


end
