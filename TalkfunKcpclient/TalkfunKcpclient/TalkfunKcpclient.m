//
//  TalkfunKcpclient.m
//  CloudLive
//
//  Created by 莫瑞权 on 2019/11/14.
//  Copyright © 2019 Talkfun. All rights reserved.
//

#import "TalkfunKcpclient.h"
#import <Kcpclient/Kcpclient.h>
@interface TalkfunKcpclient ()
@property(nonatomic,assign)BOOL isProxyThread;
@property (nonatomic,strong)NSThread *proxyThread ;
@property (nonatomic,strong)NSDictionary*  rtmp;
@property(nonatomic,assign)BOOL  isRun;
@end
@implementation TalkfunKcpclient
- (void)stopTalkfunKcpclientFramework
{

    if (self.isOpen) {
    self.isOpen = NO;
    [self stopProxy];
    }
}
- (void)loadSDK
{
        
   
    
   
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopProxy) name:@"stopTalkfunKcpclientFramework" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushTalkfunKcpclientFramework:) name:@"pushTalkfunKcpclientFramework" object:nil];
}
#pragma mark - 监听通知
- (void)pushTalkfunKcpclientFramework:(NSNotification *)note
{
  [[NSNotificationCenter defaultCenter] postNotificationName:@"pushIsKcpclientFramework" object:nil userInfo:@{}];
    
      NSDictionary * dict = note.userInfo;
    
     self.isOpen = YES;
     [self openProxy:dict];
}
+ (instancetype)shared {
    static TalkfunKcpclient *s_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [[self alloc] init];
       
    });
    return s_instance;
}


- (void)stopProxy
{

NSLog(@"结束子线程==%@", [NSThread currentThread]);
    [self.proxyThread cancel];
    _proxyThread = nil;
    KcpclientShutdown();
}
- (void)openProxy:(NSDictionary*)rtmp
{
    self.rtmp  = rtmp;
    self.proxyThread = [[NSThread alloc] initWithTarget:self selector:@selector(open) object:nil];
   self.proxyThread.name = @"subThread";

   [self.proxyThread start];
    
}
- (void)open
{
    NSDictionary *rtmpDict = self.rtmp[@"rtmpProxy"] ;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];

        NSString *localaddr = @"127.0.0.1:9527";
        [dict setObject:localaddr   forKey:@"localaddr"];

        NSString * remoteaddr = rtmpDict[@"ip"];
        remoteaddr =  [remoteaddr stringByAppendingString:@":"];
        remoteaddr =    [remoteaddr stringByAppendingString:rtmpDict[@"port"]];
        [dict setObject:remoteaddr       forKey:@"remoteaddr"];

        [dict setObject:rtmpDict[@"key"]?rtmpDict[@"key"]:@""     forKey:@"key"];
        [dict setObject:rtmpDict[@"crypt"]?rtmpDict[@"crypt"]:@"" forKey:@"crypt"];
        if ([[self getNumber:rtmpDict[@"nocomp"]] isEqualToString:@"1" ]) {
        [dict setObject:@(YES)     forKey:@"nocomp"];
        }else{
        [dict setObject:@(NO)     forKey:@"nocomp"];
        }

        NSString *json =  [self convertToJsonData:dict];
      if(self.isProxyThread==NO){
          self.isProxyThread = YES;
//              [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TalkfunKcpclientRunnoti) name:@"TalkfunKcpclientRun" object:nil];
        }

        NSLog(@"本地加速的子线程==%@", [NSThread currentThread]);
//        NSLog(@"本地加速json=======>%@",json);
        [self run:json];
  
    
}
- (void)TalkfunKcpclientRunnoti
{
        NSLog(@"结束子线程==%@", [NSThread currentThread]);
}
- (NSString*)getNumber:(NSObject*)obj
{
    NSString *UsersXid = @"0";
    
    if([obj isKindOfClass:[NSNumber class] ]){
        
        NSNumber  *num     = (NSNumber*) obj;
        NSNumberFormatter *tempNum = [[NSNumberFormatter alloc] init];
        
        UsersXid = [tempNum stringFromNumber:num];
    }else if([obj isKindOfClass:[NSString class] ]){
        
        NSString  *str    = (NSString*)obj;
        UsersXid = str;
    }
    
    return UsersXid;
}
- (NSString *)convertToJsonData:(NSDictionary *)params
{
  NSError *error;
   NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
   NSString *jsonString;

   if (!jsonData) {
       NSLog(@"%@",error);
   } else {
       jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
   }

   NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];

   NSRange range = {0,jsonString.length};

   //去掉字符串中的空格
   [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];

   NSRange range2 = {0,mutStr.length};

   //去掉字符串中的换行符
   [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];

   return mutStr;
    

}
-(void)run:(NSString *)json
{
    KcpclientSetConfigJson(json);
   
    if (self.isRun ==NO) {
        self.isRun = YES;
         KcpclientRun();
    }
 
}
@end
