//
//  TalkfunKcpclient.h
//  CloudLive
//
//  Created by 莫瑞权 on 2019/11/14.
//  Copyright © 2019 Talkfun. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TalkfunKcpclient : NSObject
@property(nonatomic,assign)BOOL isOpen;//是否开始了
+ (instancetype)shared;

- (void)loadSDK;

- (void)openProxy:(NSDictionary*)rtmp;
- (void)stopProxy;
@end

NS_ASSUME_NONNULL_END
